#!/usr/bin/env bash
cd "$(dirname "$0")"

BACKUP_DIR=${HOME}'/backups'
PASSWORD=$(cat ../data/config.ini | grep DB_PASSWORD | awk '{ split($0,a,"="); print a[2]}' | xargs)


tar -xvf ${HOME}/backups/backup.tar.gz

podman exec -i mariadb mariadb -u 'root' -p${PASSWORD} -e "DROP SCHEMA IF EXISTS muziekladder_db"

podman exec -i mariadb mariadb --user root --password=${PASSWORD} < backup

rm backup
