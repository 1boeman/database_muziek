#!/bin/env bash

PASSWORD=$(cat ../data/config.ini | grep DB_PASSWORD | awk '{ split($0,a,"="); print a[2]}' | xargs)
podman exec -i mariadb mariadb --user root --password=${PASSWORD} muziekladder_db < ./muziekladder_db.sql
podman exec -i mariadb mariadb -u 'root' -p${PASSWORD} -e "CREATE USER IF NOT EXISTS 'db_user'@'%' IDENTIFIED BY '${PASSWORD}';"
podman exec -i mariadb mariadb -u 'root' -p${PASSWORD} -e "CREATE USER IF NOT EXISTS 'db_user'@'localhost' IDENTIFIED BY '${PASSWORD}';"
podman exec -i mariadb mariadb -u 'root' -p${PASSWORD} -e "grant all privileges on muziekladder_db.* to 'db_user'@'%';"
podman exec -i mariadb mariadb -u 'root' -p${PASSWORD} -e "grant all privileges on muziekladder_db.* to 'db_user'@'localhost';"
podman exec -i mariadb mariadb -u 'root' -p${PASSWORD} -e "SET PASSWORD FOR 'db_user'@'%' = PASSWORD('${PASSWORD}');"
podman exec -i mariadb mariadb -u 'root' -p${PASSWORD} -e "SET PASSWORD FOR 'db_user'@'localhost' = PASSWORD('${PASSWORD}');"
podman exec -i mariadb mariadb -u 'root' -p${PASSWORD} -e "FLUSH PRIVILEGES"




#CREATE USER IF NOT EXISTS 'db_user'@'localhost' IDENTIFIED BY '';
#g



#CREATE USER IF NOT EXISTS 'db_user'@'localhost' IDENTIFIED BY '';
#grant all privileges on muziekladder_db.* to 'db_user'@'localhost';

#CREATE USER IF NOT EXISTS 'db_user'@'%' IDENTIFIED BY 'password';
#grant all privileges on muziekladder_db.* to 'db_user'@'%';
# docker exec -i mariadb mariadb --user db_user --password=password muziekladder_db < ./muziekladder_db.sql
