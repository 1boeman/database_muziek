#!/usr/bin/env bash
cd "$(dirname "$0")"

BACKUP_DIR=${HOME}'/backups'
PASSWORD=$(cat ../data/config.ini | grep DB_PASSWORD | awk '{ split($0,a,"="); print a[2]}' | xargs)

podman exec mariadb mariadb-dump --all-databases -uroot -p"$PASSWORD" > backup
tar -czvf backup.tar.gz backup 
rm backup
mv backup.tar.gz ${BACKUP_DIR}
