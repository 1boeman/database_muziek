PASSWORD=$(cat ../data/config.ini | grep DB_PASSWORD | awk '{ split($0,a,"="); print a[2]}' | xargs)
#13/01/2025
podman exec -i mariadb mariadb -u 'root' -p${PASSWORD} -e "alter table muziekladder_db.venue add column venue_deactivated DATETIME NULL"
