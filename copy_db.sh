podman exec -i mariadb mariadb-admin -u root -f drop muziekladder_db1
podman exec -i mariadb mariadb-admin -u root create muziekladder_db1
podman exec -i mariadb mariadb -u 'root' -e "ALTER DATABASE muziekladder_db1 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
podman exec -i mariadb mariadb -u 'root' -e "grant all privileges on muziekladder_db1.* to 'db_user'@'%';"
podman exec -i mariadb mariadb-dump -u root muziekladder_db > /tmp/oldDb.sql
podman exec -i mariadb mariadb -u root  muziekladder_db1 < /tmp/oldDb.sql
