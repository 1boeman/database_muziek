CREATE DATABASE IF NOT EXISTS muziekladder_db;
ALTER DATABASE muziekladder_db CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CREATE USER IF NOT EXISTS 'db_user'@'localhost' IDENTIFIED BY 'password';
-- grant all privileges on muziekladder_db.* to 'db_user'@'localhost';

-- CREATE USER IF NOT EXISTS 'db_user'@'%' IDENTIFIED BY 'password';
---grant all privileges on muziekladder_db.* to 'db_user'@'%';
